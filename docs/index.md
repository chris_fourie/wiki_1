# Collaborative Notes

Use local MarkDown editor to make online collaborative notes... comfortably and quickly...    

looks like this:

![screenshot](https://i.gyazo.com/51d8f2b7a650a132148f6f1d2216027f.png)

For documentation on the base project read this post [Taking notes with MkDocs](https://shinglyu.com/web/2018/01/02/taking-notes-with-mkdocs.html) and visit [mkdocs.org](http://mkdocs.org)  

## Getting started

* Get Git for your system [GIT](https://git-scm.com/)
* Make a repos folder -> right click and Open Terminal here / Git Bash here 
* `git clone https://gitlab.com/chris_fourie/wiki_1.git` or <br/>`git clone git@gitlab.com:chris_fourie/wiki_1.git`
* Get a MarkDown editor -> [best MarkDown editors](https://www.slant.co/search?query=markdown)
* write your notes in any .md file and then run `./git_update` to update the repository and deploy to the site online 
  * Add commit message with following format <br>`./git_update "main short message" "addtional detail long message" ` 
  * Nest .md files in folders to keep stuff organized 

## To edit and preview locally 

* `pip install mkdocs`
* `cd <file_path>/colab_notes`
* `mkdocs serve`
* follow link [127.0.0.1:8000](127.0.0.1:8000)

## Internal Links

* `[text to display](../folder/file.md)` -> link to file   
* `[text to display](../folder/file.md#heading-x)` -> link to paragraph ==(#lower-case-and-dashes)==
* For sharing paragraph use link icon at end of heading (hover mouse and it appears)
* More details: [mkdocs.org/user-guide/writing-your-docs/#internal-links](https://www.mkdocs.org/user-guide/writing-your-docs/#internal-links)

## Add images 

* `![text to display](../folder/imgFile.md)`

* `![text to display](imgLink)` -> Recommend using [Gyazo](https://gyazo.com) to add images/screen shots - just way easier 

* More details: [mkdocs.org/user-guide/writing-your-docs/#linking-to-images-and-media](https://www.mkdocs.org/user-guide/writing-your-docs/#linking-to-images-and-media)

  

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
    	index.md  # The documentation homepage.
    docs/folder_1/
    		notes_1,1.md
    		notes_1,2.md
    docs/folder_2/
    		notes_2.md 

## TODO

* Try get a vote button working to roughly gauge value of sections -> [example](https://codepen.io/FoFo_FoSho/pen/gOYZedG) 

***

[Markdown Tips](https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet)


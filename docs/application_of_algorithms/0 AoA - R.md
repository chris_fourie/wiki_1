# AoA - R

Application of Algorithms - Resources 

* Stanford Coursera - [[Alogrithms specialization]](https://www.coursera.org/specializations/algorithms) 4 courses, free to audit   
* MIT OCW - [[Introduction to Algorithms]](https://www.youtube.com/watch?v=HtSuA80QTyo&list=PLUl4u3cNGP61Oq3tWYp6V_F-5jb5L2iHb) 47 vids, ave 50min each 
* Video lectures aggregator - [(",)](http://videolectures.net/mit6046jf05_introduction_algorithms/)  
* XoaX - [[video tutorials]](http://xoax.net/comp_sci/crs/algorithms/index.php)
* Rashid bin Mohammed [[wiki - comprehensive]](http://www.personal.kent.edu/~rmuhamma/Algorithms/algorithm.html)
* MIT OCW - [Introduction to Algorithms](https://player.fm/series/introduction-to-algorithms-2005-15478) audio ? 23 lectures ave 1h each 
* Erik Demane - [multiple algos courses](https://erikdemaine.org/classes/)
*  Text book answers - easier to find [wiki](https://walkccc.github.io/CLRS/Chap14/14.1/)
*  Text book - [html](http://staff.ustc.edu.cn/~csli/graduate/algorithms/book6/chap15.htm)
*  





## TODO

Binary search trees https://www.youtube.com/watch?v=9Jry5-82I68


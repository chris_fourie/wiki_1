# Ch 17 - Amortized Analysis

Ideas of amortization comes from banking and credit 

[lec 7](#lec-7)





* Cormen [[chapter]](http://staff.ustc.edu.cn/~csli/graduate/algorithms/book6/chap18.htm)
* G4G [[article - 5m]](https://www.geeksforgeeks.org/analysis-algorithm-set-5-amortized-analysis-introduction/)
* YouTube, Bill Byrne [[vid - 35m]](https://www.youtube.com/watch?v=B3SpQZaAZP4)
* MIT OCW [[vid - 1h15]](https://www.youtube.com/watch?v=3MpzavN3Mco)
* Coursera, UC San Diego [[MOOC - Aggregate Method]](https://www.coursera.org/lecture/data-structures/amortized-analysis-aggregate-method-hzlEI)
* Brilliant [[article - 15m]](https://brilliant.org/wiki/amortized-analysis/)
* [faculty.cs.tamu.edu](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&ved=2ahUKEwiGzIjN2v_kAhX-ShUIHVB7DCMQFjADegQIAhAC&url=http%3A%2F%2Ffaculty.cs.tamu.edu%2Fklappi%2Fcsce411-s17%2Fcsce411-amortized3.pdf&usg=AOvVaw3uScyZkg8IEP_oN7Kwn4Ci) [[lec - 10m]](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=13&ved=2ahUKEwiGzIjN2v_kAhX-ShUIHVB7DCMQFjAMegQIBhAC&url=https%3A%2F%2Fwww.cs.cmu.edu%2F~avrim%2F451f11%2Flectures%2Flect0922.pdf&usg=AOvVaw3CGbqgAU9t2EunvcZEMIW6)

![img](Ch 17 - Amortized Analysis.assets/005722662_1-97a4b53da574f24b33ba0df4b6a0d400-1570094741523.png)

![img](Ch 17 - Amortized Analysis.assets/Incrementing+a+binary+counter.jpg)

![img](Ch 17 - Amortized Analysis.assets/slide_12.jpg)

![img](Ch 17 - Amortized Analysis.assets/AmortizedAnalysis.png)

![img](Ch 17 - Amortized Analysis.assets/images.png)

![img](https://files.transtutors.com/test/qimg/d6692d6c-6ac2-4688-a038-b18745db2c57.png)

![img](Ch 17 - Amortized Analysis.assets/images-1570094755526.png)



Binary counter

* G4G [[article - 5m]](https://www.geeksforgeeks.org/amortized-analysis-increment-counter/)
* YouTube [[vid - 5m]](https://www.youtube.com/watch?v=lMriDMovT8o)

![img](Ch 17 - Amortized Analysis.assets/images-1570094780403.png)

![img](Ch 17 - Amortized Analysis.assets/Aggregate+Method_+Binary+Counter+++Increment.jpg)

Dynamic Tables 

* (Ch 17 - Amortized Analysis.assets/Amortized+Analysis,+Amortized+cost.jpg)

![img](Ch 17 - Amortized Analysis.assets/Amortized+Analysis,+Amortized+cost-1570095592147.jpg)

![Related image](Ch 17 - Amortized Analysis.assets/1_okxxc6FfkWzbzrZyl0mtAg.png)

# Lec 7

![Related image](Ch 17 - Amortized Analysis.assets/007533951_2-60636c00cf0f7cbe82ade931f4bc2c45.png)


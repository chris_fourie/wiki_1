# Ch 6 - Heapsort (M)

Just for the masters students - probably exam question related 

* MIT OCW - https://www.youtube.com/watch?v=B7hVxCmfPtM 52min - first 20min (5/5)

## properties of heapsort

* Running time:  $O(n \lg n)$ 
* Sorts in place -> only a constant number of array elements are stored outside of the input array at any time 
* Combines the better attributes of merge sort *(running time)* and insertion sort *(sort in place)*
*  Uses **heap** data structure to manage information -> makes an efficient *priority queue* 

*the heap term was originally coined in the context of heapsort but has since come to refer to 'garbage-collected storage' in programming languages - here it is a data structure rather than an aspect of garbage collection*

## A Heap

* HackerRank - [[video overview with code]](https://www.youtube.com/watch?v=t0Cq6tVNRBA) 10min 

* An array represented as a partially / fully complete binary tree 
* Two types: max-heaps and min-heaps
* Height is *(int)*$\Theta(\lg n)$ or the number of edges from the root to the furthest leaf node. The height of a node is the number of edges from that node to a leaf node. 

General Definition of a heap - where *i* is the index in the array 

![1571398771511](Ch 6 - Heapsort.assets/1571398771511.png)

Depiction of a Min Heap -> general definition + min heap property 

* min heap property: the key*(i.e. value)* of a node is $\leq$ the key of it's children 
* max heap property:  the key*(i.e. value)* of a node is $\geq$ the key of it's children 

![1571398981616](Ch 6 - Heapsort.assets/1571398981616.png)

![1571407793018](Ch 6 - Heapsort.assets/1571407793018.png)

## Max-Heapify
![1571407814913](Ch 6 - Heapsort.assets/1571407814913.png)

![1571408089186](Ch 6 - Heapsort.assets/1571408089186.png)

![1571410040672](Ch 6 - Heapsort.assets/1571410040672.png)

## Build-Max-Heap
![1571410351387](Ch 6 - Heapsort.assets/1571410351387.png)

![1571410895419](Ch 6 - Heapsort.assets/1571410895419.png)

## Heapsort

![1571411005873](Ch 6 - Heapsort.assets/1571411005873.png)
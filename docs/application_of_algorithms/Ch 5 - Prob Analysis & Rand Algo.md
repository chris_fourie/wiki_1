# Ch 5 - Prob Analysis & Rand Algo

Probabilistic Analysis and Random Algorithms 

## The Hiring Problem

*Maintaining a Current Winner Problem*

![1570798672475](Ch 5 - Prob Analysis & Rand Algo.assets/1570798672475.png)

* Total cost: $O\left(c_{i} n+c_{h} m\right)$ where **n** = number interviewed and **m** = number hired 
* Worst-case: hire every candidate
  * occurs if candidate com in strictly increasing order of quality $\therefore$ m=n and $O\left(c_{h} n\right)$ 
  * 




# Ch 13 - Red-Black Trees

Balanced Binary Search Trees 

* Michael Sambol - [video overview](https://www.youtube.com/watch?v=qvZGUFHWChY) 4min (4/5) 
* Stanford Cousera - [video](https://www.coursera.org/learn/algorithms-graphs-data-structures/lecture/8acpe/red-black-trees) 21min 

## Properties

* Basic dynamic-set operation complexity  $=$ $O(treeHeight)$ $=$ $O(\lg n)$ time in the worst case 
* The longest simple path will only ever be 2x length of the shortest simple path 

## Invariants

* Nodes red or black (1bit)
* root is black 
* ever lead (null) is black
* red nodes have black children 
* all simple paths from nodes to descendant leaves contain the same number of black nodes 

## Rotation

* 


# 0 RL- R

## Reinforcement Learning - Resources

## Book

* [('^__^')](http://incompleteideas.net/book/the-book-2nd.html ) Sutton and Barto (4.7/5)

  * Solutions 

    * [Chap 1 - 9](http://fumblog.um.ac.ir/gallery/839/weatherwax_sutton_solutions_manual.pdf) ( /5)
    * [Chap 1,2, 13](https://micahcarroll.github.io/learning/2018/05/17/sutton-and-barto-rl.html) (with summaries) ( /5)
    * [Chap 1-3](https://github.com/iamhectorotero/rlai-exercises) (with code) ( /5)
    
    * [Chap 1-3](https://github.com/JKCooper2/rlai-exercises/tree/master/Chapter%202) (with code) ( /5)
  * Code (of content) 
    *  [Chap 1 - 13, Python](https://github.com/ShangtongZhang/reinforcement-learning-an-introduction) ( /5)
    *  [Chap 1 - 13, Lisp](http://incompleteideas.net/book/code/code2nd.html) ( /5)
  
    

## Course 

* [(^_^)](http://www.wildml.com/2016/10/learning-reinforcement-learning/) Wild ML (Denny Britz) - Meta-course (recommended reading, exercises from other courses, implementations) with implementations, Python ( /5)  

* [(^_^)](http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching.html) Dave Silver ( /5)

* [(-)](https://www.coursera.org/learn/practical-rl/home/)Coursera - Practical RL ( /5)

* [(^_^)](https://www.coursera.org/specializations/reinforcement-learning) Coursera - University of Alberta - 4 part specialization (3/5) *paid

* [(^_^)](https://courses.edx.org/courses/course-v1:Microsoft+DAT257x+2T2019/course/) EDx - Microsoft ( /5)

* [(0_o)](http://web.stanford.edu/class/cme241/) Stanford - RL for finance ( /5)

  

## Video

* [(^_^)](https://deeplizard.com/learn/video/nyjbcRQ-uQ8) Deeplizard - RL playlist - 18 Vids ( /5)
* [(^_^)](https://www.youtube.com/results?search_query=reinforcement+learning+playlist) YouTube playlist ( /5)

***

## Articles

* [:happy:](https://www.freecodecamp.org/news/an-introduction-to-reinforcement-learning-4339519de419/) Free code camp - ( 4/5) - *good overview* 
* [(",)](https://towardsdatascience.com/introduction-to-various-reinforcement-learning-algorithms-i-q-learning-sarsa-dqn-ddpg-72a5e0cb6287) Medium, RL algorithm review (3/5)
* [(",)](https://medium.com/@SmartLabAI/reinforcement-learning-algorithms-an-intuitive-overview-904e2dff5bbc) Medium, RL overview (3/5) 
* [(",)](https://medium.com/@jonathan_hui/rl-basics-algorithms-and-terms-ae98314851d7) Summary of terminology (3.5/5)

## Lectures

* [(",)](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=17&ved=2ahUKEwi_i5G_1YHlAhVUTsAKHe7-DO0QFjAQegQIBhAC&url=https%3A%2F%2Fwww.cs.cmu.edu%2F~mgormley%2Fcourses%2F10601-s17%2Fslides%2Flecture26-ri.pdf&usg=AOvVaw16vK6xMu1KnDfSBJLhsQ2h) 55 slides (3.5/5)
* [(",)](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=16&ved=2ahUKEwi_i5G_1YHlAhVUTsAKHe7-DO0QFjAPegQIAhAC&url=http%3A%2F%2Fgki.informatik.uni-freiburg.de%2Fteaching%2Fws0607%2Fadvanced%2Frecordings%2Freinforcement.pdf&usg=AOvVaw27GhncdcRKcuC4kef1j_m1) 160 slides (3/5)
* 

***

## Taxonomy of RL algorithms 

![](https://spinningup.openai.com/en/latest/_images/rl_algorithms_9_15.svg)

## Prediction problem

..

## Control problem

![](0 RL - R.assets/a8e1ccdd61df1fcc9c81d1478803261a.png)

## Back up digrams 

* [(0__o)](https://towardsdatascience.com/all-about-backup-diagram-fefb25aaf804) medium, article - 6m (2.5/5)

  ![](0 RL - R.assets/Full+and+Sample+(One-Step)+Backups.jpg)

## Example questions / exams

* Stanford - RL for finance [exam](http://web.stanford.edu/class/cme241/lecture_slides/final.pdf) [solutions](http://web.stanford.edu/class/cme241/lecture_slides/final-solutions.pdf) ( /5)
* 


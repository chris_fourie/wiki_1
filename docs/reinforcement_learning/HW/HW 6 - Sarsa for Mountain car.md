# HW 6 - Sarsa for mountain car

* [stackoverlfow](https://stackoverflow.com/questions/45377404/episodic-semi-gradient-sarsa-with-neural-network)
* [Michael Oneil - tutorial](https://michaeloneill.github.io/RL-tutorial.html)
* [O Reilly](https://www.oreilly.com/library/view/reinforcement-learning-in/10000MNLV201807/) [video](https://www.oreilly.com/library/view/reinforcement-learning-in/10000MNLV201807/tabor_u08m08.html)
* 
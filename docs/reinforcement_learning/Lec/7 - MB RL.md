# 7 - MB - RL

Model Based - RL

* [Difference betwen model bases and model free](https://ai.stackexchange.com/questions/4456/whats-the-difference-between-model-free-and-model-based-reinforcement-learning) - look at second answer 
* David Silver - [vid lec 8: intergrated learning and planning](https://www.youtube.com/watch?v=ItMutbeOHtc) , [lecture pdf](http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching_files/dyna.pdf)



## overview 

* learning a model = learning a model of the MDP = learning a model of the environment 

* What does the environement do?

  * Transision dynamics 
  * Rewards 

* Pros: 

  * can efficiently learn a model using supervised learning 
  * reason about model uncertainty -> choosed to take actions to states where environment is not well understood -> aids with exploration 

* Cons: 

  * Two sources of approximation error

    * Model approximation 

    * Value function approximation 

       




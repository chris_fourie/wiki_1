# 8 - PG

Policy Gradients 

* Coursera - alberta RL - vids

  * [useful intro](https://www.coursera.org/learn/prediction-control-function-approximation/lecture/6JjWl/learning-policies-directly) vid 15min (3/5)

* [David Silver Lec 7](https://www.youtube.com/watch?v=KHZVXao4qXs) vid  1h 1min 

  * Watch this    (5/5)

  

![1573561210103](8 - PG.assets/1573561210103.png)



## Wild ML Summary

* [Wild ML - Policy Gradients](https://github.com/dennybritz/reinforcement-learning/tree/master/PolicyGradient) 

![1573550272030](8 - PG.assets/1573550272030.png)

![1573552833737](8 - PG.assets/1573552833737.png)



![1573562022365](8 - PG.assets/1573562022365.png)

## Important terms

### Objective Functions

![1573571724362](8 - PG.assets/1573571724362.png)



![](8 - PG.assets/1573563692868.png)

### Policy Gradient

![1573571784297](8 - PG.assets/1573571784297.png)

### Numerical method - Finite Differences

![1573563736941](8 - PG.assets/1573563736941.png)

# Policy Gradient Al

![1573568817691](8 - PG.assets/1573568817691.png)

### Score Function

![1573571833455](8 - PG.assets/1573571833455.png)

## Analytical methods

### Monte Carlo

![1573571916380](8 - PG.assets/1573571916380.png)

![1573572312726](8 - PG.assets/1573572312726.png)

![1573572330923](8 - PG.assets/1573572330923.png)

### TD

![1573572348995](8 - PG.assets/1573572348995.png)

![1573572363921](8 - PG.assets/1573572363921.png)

![1573572382633](8 - PG.assets/1573572382633.png)

### Baselines

![1573564706291](8 - PG.assets/1573564706291.png)

![1573564620603](8 - PG.assets/1573564620603.png)

![1573564641955](8 - PG.assets/1573564641955.png)

![1573572402513](8 - PG.assets/1573572402513.png)

 ### other resources to consider 

- article - https://medium.com/@ts1829/policy-gradient-reinforcement-learning-in-pytorch-df1383ea0baf
- denny britz implementation - https://github.com/dennybritz/reinforcement-learning/blob/master/PolicyGradient/CliffWalk%20REINFORCE%20with%20Baseline%20Solution.ipynb 
- implementation - https://github.com/seungeunrho/minimalRL/blob/master/REINFORCE.py
- technical article and implementation - https://medium.com/@thechrisyoon/deriving-policy-gradients-and-implementing-reinforce-f887949bd63 
- def reinforce - https://www.datahubbs.com/reinforce-with-pytorch/
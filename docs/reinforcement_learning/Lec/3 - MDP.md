# 3 - MDP

Markov Decision Process 

* [wild ai](https://github.com/dennybritz/reinforcement-learning/tree/master/MDP)
* 

![1573670675584](3 - MDP.assets/1573670675584.png)

![1573673854867](3 - MDP.assets/1573673854867.png)

## Back up diagrams

https://towardsdatascience.com/all-about-backup-diagram-fefb25aaf804

![img](3 - MDP.assets/0_8ey1DgP4IDXyoqKQ.png)



![1573672029010](3 - MDP.assets/1573672029010.png)

![1573672155029](3 - MDP.assets/1573672155029.png)

![1573672192822](3 - MDP.assets/1573672192822.png)

![1573672707468](3 - MDP.assets/1573672707468.png)

## Markov Property

![1573674423862](3 - MDP.assets/1573674423862.png)

##  <S, A, P, R, $\gamma$>

### State space 

![1573674964538](3 - MDP.assets/1573674964538.png)

![1573674989824](3 - MDP.assets/1573674989824.png)

### Transition function

![1573674240689](3 - MDP.assets/1573674240689.png)

### Discount

![1573674010207](3 - MDP.assets/1573674010207.png)

![1573674097537](3 - MDP.assets/1573674097537.png)
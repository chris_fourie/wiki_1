# 5 - Model Free - MC & TD

* Article (4/5) - https://medium.com/deep-math-machine-learning-ai/ch-12-1-model-free-reinforcement-learning-algorithms-monte-carlo-sarsa-q-learning-65267cb8d1b4

![1573551207062](5 - TD.assets/1573551207062.png)



## Difference between Monte Carlo and Temporal Difference

![1573561002956](5 - TD.assets/1573561002956.png)

![](https://i.gyazo.com/9fbbb2d0e38b2db41d516fd1aa699ad5.png)

![1570108532351](5 - MF RL.assets/7f91b1262e3b5ee58eb49755a173ecfb.png) 

[[ref]](https://www.freecodecamp.org/news/an-introduction-to-reinforcement-learning-4339519de419/)

![](https://i.gyazo.com/994a334901e597b95cdf79dcf6b88c01.png)

https://datascience.stackexchange.com/questions/26938/what-exactly-is-bootstrapping-in-reinforcement-learning

![1570108032063](5 - MF RL.assets/1fad6f7cc6add6d76387e54b9772f51c.png) <br>

## Learning Signal

TD error ($\delta$) = learning signal 

![](5 - TD.assets/bf7848e9f8a6a2b0a7dc232e910241b7.png)

![](5 - TD.assets/a762b9bda5bb5aaad2bc2dc0b9595a8c.png)



TD learns from **Experience** *e~t~=(s~t~,a~t~,r~t~,s~t+1~)*

***

## Monte Carlo 

![1573672891472](5 - TD.assets/1573672891472.png)

![1573672939176](5 - TD.assets/1573672939176.png)

![1573672972270](5 - TD.assets/1573672972270.png)

## TD

![1573673052494](5 - TD.assets/1573673052494.png)

### Prediction

### Control 

Use TD learning to update **Q-value Function** 

![img](5 - TD.assets/7fe33e3301a6564aa0de797356d26104-1570113543059.png)

2 Methods 

* SARSA -> selects **A~t+1~** according to policy that selected **A~t~** *(on-policy)* - follows 'safest 'path 

  ![img](https://i.gyazo.com/f020e34bdd05f48bd1342c74f6174632.png)

  ![](5 - TD.assets/5b0624381d019680b2faa546011a931d.png)

  <br>

  <br>

* Q-learning ->  selects **A~t+1~** according to **Greedy policy** (off-policy) - follows optimal path 

  ![](5 - TD.assets/f6148da110aae10874d83e84dbfc3c75.png)

  ![](5 - TD.assets/7ef8d250a9257a5333185b9f91ab919d.png)



***

## n-steps

![](5 - TD.assets/13e106dc27af5523ddc9f177b610973b.png)

![](5 - TD.assets/8c5642a37392aafe454379264419dc1a.png)

![](5 - TD.assets/e16eda47f78a489a32c534383de9d902.png)

## TD ($\lambda$)

More computation but better estimates of $V~\pi~$



![](5 - TD.assets/d28555f37144d11a51f0ac02ce69d9dd.png)

![](5 - TD.assets/c28b994b6784393593ff9aeda428acb3.png)



![](https://i.gyazo.com/41701b1d856fe27bdf1f25460684585c.png)

![](https://i.gyazo.com/8ec8f1f12dc36b9d289eadbd042e34f8.png)

![](5 - TD.assets/3a0652db02a508fe16a427c17e4f8d85.png)

![](5 - TD.assets/c5d828f10ba84703d1c0de4a79c1a14f.png)

![](5 - TD.assets/d373ea1160648209775662da7dbf6dfc.png)

### Eligibility traces

* [0__o](https://web.fe.up.pt/~eol/schaefer/diplom/ReinforcementLearning.htm) Article - see bottom 

![](5 - TD.assets/1b9b113faa795ce13bb74c7b6089b98d.png)

![](5 - TD.assets/03cc6b3aa9098ccd4257dda76b5ad7a0.png)

* **1 or 0** - for frequency -> 1 if state has been visited 
* **Gamma and lamda** for recency  

![](5 - TD.assets/33b8c6479a8dd982c33d10d5d2742c5e.png)

### Prediction 

![](5 - TD.assets/1f3c4ccab434f8a13d2d0835bbef3032.png)
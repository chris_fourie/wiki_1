# 9 - HRL

* Article - https://thegradient.pub/the-promise-of-hierarchical-reinforcement-learning/
* good-ish vid - https://youtu.be/zQy02LsARo0?t=347
  * paper https://arxiv.org/abs/1803.00590 
* Example - https://openai.com/blog/learning-a-hierarchy/
* NeurIPS 2017 HRL workshop play list - https://www.youtube.com/watch?v=QEmuhofpFIU&list=PLfsVAYSMwskuaQ7fxEJaBZcZ2aXyc-5K-
  * Good scope on this vid - https://www.youtube.com/watch?v=vx7i1rkthEE&list=PLfsVAYSMwskuaQ7fxEJaBZcZ2aXyc-5K-&index=2 
* vid to try - https://www.youtube.com/watch?v=e8b0yC6COJ8





* ![img](9 - HRL.assets/31d8ddcdf3fd52b3ce3b6edeaa1e4a1a.png)From Text book 



## Lecture 

![1573496083106](9 - HRL.assets/1573496083106.png)





![1573496115189](9 - HRL.assets/1573496115189.png)

![1573496160355](9 - HRL.assets/1573496160355.png)

![1573496179541](9 - HRL.assets/1573496179541.png)

![1573496244200](9 - HRL.assets/1573496244200.png)

![1573496266688](9 - HRL.assets/1573496266688.png)

![1573496284688](9 - HRL.assets/1573496284688.png)

![1573496316729](9 - HRL.assets/1573496316729.png)

![1573496408400](9 - HRL.assets/1573496408400.png)

![1573496423430](9 - HRL.assets/1573496423430.png)
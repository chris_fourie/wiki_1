# 4 - DP

* [:happy:](https://stackoverflow.com/questions/37370015/what-is-the-difference-between-value-iteration-and-policy-iteration) stackoverflow, difference between policy iteration and value iteration 
  * **Policy iteration** includes: **policy evaluation** + **policy improvement**
  * **Value iteration** includes: **finding optimal value function** + one **policy extraction**
  * **Finding optimal value function**
    can also be seen as a combination of policy improvement (due to max) 
    and truncated policy evaluation (the reassignment of v_(s) after just 
    one sweep of all states regardless of convergence).
  * 
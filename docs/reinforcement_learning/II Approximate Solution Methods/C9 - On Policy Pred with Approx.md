# C9 - On Policy Pred with Approx

On policy prediciton with a approxiamtion 

Functional Approximation (FA)

* Wits Lec - 6: Funcional approximation [get slides moodle](), [no video]()
* David Silver - Lec 6: Value function approximation [[slides]](http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching_files/FA.pdf), [[video]](https://www.youtube.com/watch?v=UoPei5o4fps)
* Denny Britz [[repo]](https://github.com/dennybritz/reinforcement-learning/tree/master/FA)
* EdX - Microsoft [[mooc]](https://courses.edx.org/courses/course-v1:Microsoft+DAT257x+2T2019/courseware/a4d67a65-7ee1-68a5-52be-5349f0a3268c/9d3362b6-7f55-c0b4-5ba4-acf9fa528305/1?activate_block_id=block-v1%3AMicrosoft%2BDAT257x%2B2T2019%2Btype%40vertical%2Bblock%401467ff02-0dbc-531a-4608-b7a23671b2bf)
  * Linear function approximators [[video]](https://courses.edx.org/courses/course-v1:Microsoft+DAT257x+2T2019/courseware/a4d67a65-7ee1-68a5-52be-5349f0a3268c/8b656c74-a314-be36-89f2-9245ea046a51/?child=first)
* Sutton - [[lvideo lecture / tutorial]](https://youtu.be/ggqnxyjaKe4?t=3236) (2h20min, start at 56min for FA)




